var friendList;

$(document).ready(function() {
    $('#input-login-id').keypress(function(e){
        if(e.keyCode==13){
        	$('#button-login').click();
        	e.preventDefault();
        }
      });
    
	$("#button-login").click(function(){
		var cid = $('#input-login-id').val();
		$.ajax({
			  method: 'POST',
			  url: 'http://ec2-54-199-122-212.ap-northeast-1.compute.amazonaws.com/v1/verify',
			  data: { 'cid' : cid.toUpperCase()},
			}).done(function(data) {
			 if(data.result == 0) {
				 $("#id-cid").val(cid);
				 $.get('user-list.html').success(function(data) {
	                 $('#content').html(data);
	             }); 
			 }
			 else 
				 alert("Your login id is not valid!");
			 }).fail(function(){
				 alert("Your login id is not valid!")
			 })
	});
	
	$('#friend-list-table-id.friend-list-tr').click(function(){
        var row = $(this).index();
        alert(row);
    });
});
function backToRoomList(){
	$.get('room-list.html').success(function(data) {
        $('#content').html(data);
    }); 
}
function getFriendNickname(puid){
	var name = ""
	$.each(friendList, function(k, friend){
		if(friend.puid == puid){
			name =  friend.nickname;
			return false;
		}
	});
	return name;
}
function getFriendAvtSrc(puid){
	var avt = ""
	$.each(friendList, function(k, friend){
		if(friend.puid == puid){
			avt =  friend.avatar;
			return false;
		}
	});
	return avt;
}
function toDisplayDateFormat(input){
	var fullDate = new Date(input);
	var date = fullDate.getUTCDate();
	var month = fullDate.getUTCMonth() + 1;
	var hour = fullDate.getUTCHours();
	var min = fullDate.getUTCMinutes();
	return month + "/" + date + "<br>" + hour + ":" + min
	console.log(date);
}

